#!/usr/bin/env python3

import os
import sys
import sqlite3
import time
import string
import random
import shutil

from pathlib import Path

bbspath = '/home/andrew/bbs'
wwwhost = 'https://happylandbbs.com'

def randStr(chars = string.ascii_uppercase + string.digits, N=10):
	return ''.join(random.choice(chars) for _ in range(N))

if len(sys.argv) < 3:
    sys.stderr.write("Usage webdl.py /path/to/filetodl /path/to/noddy_downloads.sqlite3")
    exit()

con = sqlite3.connect(sys.argv[2])
cur = con.cursor()

cur.execute("CREATE TABLE IF NOT EXISTS downloads(hash TEXT, file TEXT, expiry INTEGER);")
hashstr = randStr(N=25)
expiry = int(time.time()) + 86400

if os.path.abspath(sys.argv[1]).startswith(os.path.abspath(bbspath+'/temp')+os.sep) or os.path.abspath(sys.argv[1]).startswith(os.path.abspath(bbspath+'/scripts/data')+os.sep):
    Path(bbspath+"/temp/noddydl/" + hashstr).mkdir(parents=True, exist_ok=True)
    shutil.copyfile(sys.argv[1], bbspath+"/temp/noddydl/"+hashstr+"/"+os.path.basename(sys.argv[1]))
    filestr = bbspath+"/temp/noddydl/"+hashstr+"/"+os.path.basename(sys.argv[1])
else:
    filestr = sys.argv[1]
    
cur.execute("INSERT INTO downloads (hash, file, expiry) VALUES(?, ?, ?)", (hashstr, filestr, expiry))

con.commit()

con.close()

print ("\r\n{}: {}/download/{}\r\n".format(os.path.basename(sys.argv[1]), wwwhost, hashstr))

